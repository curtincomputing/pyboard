class Piece():
    def __init__(self, name, x, y):
        self.name = name
        self.x = x
        self.y = y

    def __str__(self):
        return self.name + " x:" + str(self.x) + " y:" + str(self.y) 

    def move(self, x1, y1):
        self.x = x1
        self.y = y1
