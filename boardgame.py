#
# boardggame - functionality of boardgame classes: Board and Piece
#
# 4/1/19   - documentation added
# 28/12/18 - game name added to input file
# 27/12/18 - added simple rules to validate moves, colours on board
# 21/12/18 - Board and Piece classes, ASCII display of board, fileIO

# colours for printing board
colours = ["\033[0;37;40m",       # black background
                 "\033[0;37;41m", # red 
                 "\033[0;30;48m"] # normal
        
class Piece():
    '''Class to hold the position and state of game piece
    including knowledge of valid moves
    '''
    
    def __init__(self, name, symbol, x, y, dir):
        self.name = name
        self.symbol = symbol
        self.x = x
        self.y = y
        self.dir = dir #direction for initial movement
        self.crowned = True

    def __str__(self):
        return self.symbol 

    def getState(self):
        return self.name + "(" + self.symbol + ")" + " x:" + str(self.x) + " y:" + str(self.y) + "dir: " + str(self.dir)

    def move(self, x1, y1):
        '''Move piece after validation
        Arguments : x (row), y (col) positions to move to'''
        self.x = x1
        self.y = y1
        if (self.dir == 1 and self.x ==7) or (self.dir == -1 and self.x ==0):
            self.dir = 0
            self.crowned = True
            self.symbol = "\033[0;33;48m" + self.symbol + "\033[0;30;48m"

    def getPos(self):
        return(self.x, self.y)
        
    def _validMove(self, pos):
        result = False
        print(self.getState())
        print(pos)
        if self.dir == 1 or self.dir == -1: # not crowned
            if (pos[0] + pos[1]) % 2 != 0:  # black square OK
                if pos[0] - self.x == self.dir: # adjacent and correct dir
                    if pos[1] - self.y == 1 or pos[1] - self.y == -1:
                        result = True
                elif pos[0] - self.x == self.dir*2: # jumping
                    if pos[1] - self.y == 2 or pos[1] - self.y == -2:
                        result = True
                    
        else: # crowned
            if (pos[0] + pos[1]) % 2 != 0:
                if pos[0] - self.x == 1 or pos[0] - self.x == -1:
                    if pos[1] - self.y == 1 or pos[1] - self.y == -1:
                        result = True
        return result

class Board():
    def __init__(self, rangex, rangey):
        self.rangeRow = rangex
        self.rangeCol = rangey
        #row = [None for i in range(self.rangey)]
        #self.board = [row.copy() for i in range(self.rangex)]
        self.board = [ [None for i in range(self.rangeCol)] for i in range(self.rangeRow)]

    def __str__(self):

        outstring = colours[0] + "    0 1 2 3 4 5 6 7 \n"+ "   +" + "-"*(self.rangeRow-1)*2 + "-" + "+"
        #outstring = outstring + "\n"
        toggle = 0
        rownum = 0 
        for row in self.board:
            outstring = outstring + "\n" + str(rownum) + "  |"
            for square in row:
                toggle = toggle * -1 + 1
                if square:
                    outstring = outstring + colours[toggle] + str(square) + colours[0] + "|"
                else:
                    outstring = outstring + colours[toggle] + " " + colours[0] + "|"
            #outstring = outstring + "\n|"
            toggle = toggle * -1 + 1
            rownum = rownum + 1
        outstring = colours[0] + outstring[:-1] + "|\n   +" + "-"*(self.rangeRow-1)*2 + "-" + "+" +  colours[2]
        return outstring
    
    def addPiece(self, name, sym, r, c, dir):
        piece = Piece(name, sym, r, c, dir)
        self.board[r][c] = piece

    def movePiece(self, name, r, c):
        #piece = [p for p in self.board if p!=None and name==p.name]
        piece = None
        dest = (r, c)
        
        #find piece
        for row in self.board:
            for square in row:
                if square:
                    if name == square.name:
                        piece = square
                        
        #validate move if piece exists, then move it, move it
        if piece:
            pos = piece.getPos()
            # should change to exception handling for moving pieces
            if self._validMove(dest):
                if piece._validMove(dest):
                    self.board[r][c] = piece
                    self.board[pos[0]][pos[1]] = None
                else:
                    print("Error - invalid piece move")
            else:
                print("Error - invalid board move")
        else:
            print("Error - piece not found")
            
    def movePosPiece(self, fromPos, dest):
        #piece = [p for p in self.board if p!=None and name==p.name]
        piece = self.board[fromPos[0]][fromPos[1]]
                               
        #validate move if piece exists, then move it, move it
        if piece:
            #pos = piece.getPos()
            if self._validMove(dest):
                if piece._validMove(dest):
                    self.board[dest[0]][dest[1]] = piece
                    piece.move(dest[0],dest[1])
                    self.board[fromPos[0]][fromPos[1]] = None
                    print(str(piece) + " moved from " + str(fromPos) + "to" + str(dest))
                else:
                    print("Error - invalid piece move")
            else:
                print("Error - invalid board move")
        else:
            print("Error - piece not found")
            
    def _validMove(self, pos):
        result = False
        print(pos)
        if pos[0] >= 0 and pos[0] < self.rangeRow:
            print("x ok")
            if pos[1] >= 0 and pos[1] < self.rangeCol:
                print("y ok")
                if self.board[pos[0]][pos[1]] == None:
                    result = True
                else:
                    print("piece already at " + str(pos) + self.board[pos[0]][pos[1]].getState())
        return result
        

