from boardgame import *
import sys

infile = sys.argv[1]

with open(infile) as f:
    sizeLine = f.readline()
    sizex = int(sizeLine.split(",")[0])
    sizey = int(sizeLine.split(",")[1])
    pieces = f.readlines()

board = Board(sizex, sizey)

print(board)

for pieceLine in pieces:
    pieceList = pieceLine.strip().split(",")
    board.addPiece(pieceList[0], pieceList[1], int(pieceList[2]), int(pieceList[3]))

print(board)
board.movePiece("R1", 3, 3)
board.movePiece("R1", 100, 3)
board.movePiece("R1", 1, 0)
board.movePiece("R9", 3, 0)
print(board)
