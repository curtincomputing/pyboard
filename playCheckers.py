#
# playCheckers - testing functionality of boardgame classes
#
# 29/12/18 - loop for inputting moves
#
from boardgame import Board
import sys

def inputPos(prompt):
    positionStr = input(prompt)
    pos0 = int(positionStr.split(",")[0])
    pos1 = int(positionStr.split(",")[1])
    return (pos0,pos1)

if len(sys.argv) == 2:
    infile = sys.argv[1]
else:
    infile = "checkers.csv"

with open(infile) as f:
    gameLine = f.readline()
    game = gameLine.split(",")[0]
    sizeRows = int(gameLine.split(",")[1])
    sizeCols = int(gameLine.split(",")[2])
    pieces = f.readlines()

board = Board(sizeRows, sizeCols)

for pieceLine in pieces:
    pieceList = pieceLine.strip().split(",")
    if pieceList[0][0] == "R":
        board.addPiece(pieceList[0], pieceList[1], int(pieceList[2]), int(pieceList[3]), 1)
    else:
        board.addPiece(pieceList[0], pieceList[1], int(pieceList[2]), int(pieceList[3]), -1)

print(board)

done = False

while not done:
    fromPos = inputPos("Input from position...")
    toPos = inputPos("Input to position...")
    board.movePosPiece(fromPos, toPos)
    print(board)
    if input("Finished? Y/N...") == "Y":
        done = True
