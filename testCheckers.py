#
# testCheckers - testing functionality of boardgame classes
#
# 03/01/19 - added documentation and validation  
# 28/12/18 - game name added to input file, loop for inputting moves
# 27/12/18 - added simple rules to validate moves, colours on board
#
from boardgame import Board
import sys

def inputPos(prompt, min, max):
    """Read two pos integers, separated by a comma, return them as a tuple.
    
    Arguments:
    prompt -- string to prompt user for values
    min -- minimum valid value
    max -- maximum valid value
    """ 
    done = False
    while not done:
        positionStr = input(prompt)
        pos0 = int(positionStr.split(",")[0])
        pos1 = int(positionStr.split(",")[1])
        if pos0 < min or pos0 > max or pos1 < min or pos1 > max:
            print("... error, value(s) out of range")
        else: 
            done = True
    return (pos0,pos1)

infile = sys.argv[1]

with open(infile) as f:
    gameLine = f.readline()
    game = gameLine.split(",")[0]
    sizeRows = int(gameLine.split(",")[1])
    sizeCols = int(gameLine.split(",")[2])
    pieces = f.readlines()

board = Board(sizeRows, sizeCols)

print(board)
for pieceLine in pieces:
    pieceList = pieceLine.strip().split(",")
    if pieceList[0][0] == "R":
        board.addPiece(pieceList[0], pieceList[1], int(pieceList[2]), 
                       int(pieceList[3]), 1)
    else:
        board.addPiece(pieceList[0], pieceList[1], int(pieceList[2]), 
                       int(pieceList[3]), -1)

print(board)
print("\n\033[0;30;46m" + "Testing (0,7) to (1,6)" + "\033[0;30;48m")
board.movePosPiece((0,7), (1,6))
board.movePiece("R1", 100, 3)
board.movePiece("R1", 1, 0)
board.movePiece("R9", 3, 0)
print(board)

board2 = Board(sizeRows, sizeCols)
board2.addPiece("R1","R", 0, 7, 1)
board2.addPiece("B1","B", 7, 6, -1)
print(board2)
board2.movePosPiece((0,7), (1,6))
board2.movePosPiece((7,6), (6,7))
print(board2)

done = False

while not done:
    fromPos = inputPos("Input from position...", 0, sizeRows - 1)
    toPos = inputPos("Input to position...", 0, sizeRows - 1)
    if fromPos[0] - toPos[0] == 1 or fromPos[0] - toPos[0] == -1:
        board2.jumpPosPiece(fromPos, toPos)
    else:
        board2.movePosPiece(fromPos, toPos)
    
    print(board2)
    if input("Finished? Y/y...").upper() == "Y":
        done = True